
const gameContainer = document.getElementById("game")

const gifs = [
  "./friends/1.jpg",
  "./friends/2.jpg",
  "./friends/3.jpg",
  "./friends/4.jpg",
  "./friends/5.jpg",
  "./friends/6.jpg",
  "./friends/7.jpg",
  "./friends/8.jpg",
  "./friends/9.jpg",
  "./friends/10.jpg",
  "./friends/11.jpg",
  "./friends/12.jpg",
  "./friends/13.jpg",


]

let numberofcards = document.getElementById("numberofcards").value

document.getElementById("numberofcards").addEventListener('input', () => {
  numberofcards = document.getElementById("numberofcards").value
  console.log(numberofcards)
  numberofcards = parseInt(numberofcards)
  document.getElementById("numberofcards").value = parseInt(numberofcards)
  gifsWithDupes = [...gifs.slice(0, numberofcards), ...gifs.slice(0, numberofcards)]
  shuffledColors = shuffle(gifsWithDupes)
})


let gifsWithDupes = [...gifs.slice(0, numberofcards), ...gifs.slice(0, numberofcards)]


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter)

    // Decrease counter by 1
    counter--

    // And swap the last element with it
    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

let shuffledColors = shuffle(gifsWithDupes)

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  let uniqueId = 0
  for (let color of colorArray) {

    // create a new div
    const newDiv = document.createElement("div")
    const imgDiv = document.createElement('img')
    imgDiv.src = color

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color)
    newDiv.classList.add(uniqueId)
    uniqueId++

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick)

    // append the div to the element with an id of game
    newDiv.append(imgDiv)
    setTimeout(() => {
      newDiv.style.transform = 'translateY(1000px)'
      gameContainer.append(newDiv)
      setTimeout(() => {
        newDiv.style.transform = 'translateY(0)'
        setTimeout(() => {
          newDiv.style.transform = ''
        }, uniqueId * 20)
      }, uniqueId * 20)
    }, uniqueId * 50)
  }
}
document.querySelector('.completion button').addEventListener('click', () => {
  location.reload()

})
document.getElementById("home").addEventListener("click", function () {
  if (confirm("Are you sure you want to go back?")) {
    location.reload()
  }
})

// TODO: Implement this function!
let remains = 2
let targets = []
let matchedColor = []

function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  if (event.target.classList.contains('on') || remains == 0 || matchedColor.includes(event.target.classList[0])) {
    //wait
    console.log("you clicked", event.target)
    if (event.target.classList.contains('matched')) {
      event.target.classList.add('on')
    }

  } else {

    console.log("you clicked", event.target)
    event.target.style.backgroundImage = 'url(./assests/eye.png)'
    event.target.classList.add('on')

    remains--

    targets.push(event.target)
    if (targets.length == 2) {
      let colorA = targets[0].classList[0]
      let colorB = targets[1].classList[0]
      let identifierA = targets[0].classList[1]
      let identifierB = targets[1].classList[1]
      console.log(colorA, colorB)
      if (colorA == colorB && (identifierA !== identifierB)) {

        document.getElementById('score').textContent = (matchedColor.length + 1).toString()

        if (localStorage.getItem("High Score") === null) {
          localStorage.setItem("High Score", matchedColor.length + 1)
        } else {
          if (matchedColor.length + 1 > localStorage.getItem('High Score')) {

            localStorage.setItem("High Score", matchedColor.length + 1)
          }
        }

        if (matchedColor.length + 1 >= numberofcards) {
          document.querySelector('.info').style.display = 'inline-block'
          document.querySelector('.completion').style.display = 'flex'
          while (gameContainer.firstChild) {
            gameContainer.removeChild(gameContainer.firstChild)
          }
        }
        targets.map((target) => {
          target.classList.add('matched')
          target.classList.add('on')

          return target
        })

        matchedColor.push(colorA)
        targets = []
      } else {
        targets.shift()
      }
    }

    setTimeout(() => {
      if (event.target.classList.contains('matched')) {
        event.target.classList.add('on')
      } else {
        event.target.style.backgroundColor = 'lightpink'
        event.target.classList.remove('on')
        console.log(targets)
      }
      console.log(event.target.classList)
      remains++
    }, 1.5 * 1000)
  }


}
document.addEventListener('DOMContentLoaded', () => {
  // when the DOM loads
  let highscore = localStorage.getItem('High Score')

  if (highscore !== null) {

    document.getElementById('highscore').textContent = localStorage.getItem('High Score')
  } else {
    document.getElementById('highscore').textContent = 0

  }
  const start = document.querySelector('.start')
  window.addEventListener('load', () => {

    start.addEventListener('click', () => {
      if (numberofcards > 3 && numberofcards <= 13) {

        console.log(numberofcards)
        start.style.display = 'none'
        document.getElementById('home').style.display = 'block'
        document.querySelector('.info').style.visibility = 'hidden'
        document.getElementById("numberofcards").style.display = 'none'
        document.getElementById("inputerror").style.display = 'none'
        document.querySelector('.lds-roller').style.display = 'inline-block'
        const loadPromises = gifs.slice(0, numberofcards).map(gif => {
          return new Promise((resolve, reject) => {
            const img = new Image()
            img.src = gif
            img.onload = resolve
            img.onerror = reject
          })
        })
        Promise.all(loadPromises)
          .then(() => {
            document.querySelector('.info').style.display = 'none'
            document.querySelector('.lds-roller').style.display = 'none'
            createDivsForColors(shuffledColors)
          })
          .catch((err) => {
            document.querySelector('.error').style.display = 'block'
          })

      } else {
        document.querySelector('#inputerror').style.display = 'block'
      }
    })
  })

})
